//
//  SlideMenuTransitioningDelegate.swift
//  mc2testapp
//
//  Created by Никита Попов on 19/02/2019.
//  Copyright © 2019 Nikita. All rights reserved.
//

import UIKit

class SlideMenuTransitioningDelegate: NSObject, UIViewControllerTransitioningDelegate {
    func animationController(forPresented presented: UIViewController,
                             presenting: UIViewController,
                             source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return SlideMenuPresentAnimation()
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return SlideMenuDismissAnimation()
    }
}
