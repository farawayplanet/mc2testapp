//
//  SlideMenuDatasource.swift
//  mc2testapp
//
//  Created by Никита Попов on 18/02/2019.
//  Copyright © 2019 Nikita. All rights reserved.
//

import UIKit

struct Constants {
    let numberOfRows = 5
}

class SlideMenuDatasource: NSObject {
    
    private let constants = Constants()
    
    func slideMenuItems() -> [SlideMenuViewModel] {
        var items: [SlideMenuViewModel] = []
        for i in 0..<constants.numberOfRows {
            let item = SlideMenuViewModel(title: "\(i) item")
            items.append(item)
        }
        
        return items
    }
}
