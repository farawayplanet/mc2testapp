//
//  SlideMenuTableViewCell.swift
//  mc2testapp
//
//  Created by Никита Попов on 18/02/2019.
//  Copyright © 2019 Nikita. All rights reserved.
//

import UIKit

class SlideMenuTableViewCell: UITableViewCell {

    @IBOutlet private weak var titleLabel: UILabel!

    func configure(viewModel: SlideMenuViewModel) {
        titleLabel.text = viewModel.title
    }
    
}
