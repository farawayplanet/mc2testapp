//
//  SlideMenuPresentAnimation.swift
//  mc2testapp
//
//  Created by Никита Попов on 18/02/2019.
//  Copyright © 2019 Nikita. All rights reserved.
//

import UIKit

class SlideMenuPresentAnimation: NSObject, UIViewControllerAnimatedTransitioning {
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.33
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        guard let toViewController = transitionContext.viewController(forKey: .to) else {
            assert(false, "controller cannot be nil")
        }
        
        let containerView = transitionContext.containerView
        let duration = transitionDuration(using: transitionContext)
        
        toViewController.view.transform = CGAffineTransform(translationX: -containerView.bounds.width,
                                                            y: 0)
        containerView.addSubview(toViewController.view)
        
        UIView.animate(withDuration: duration,
                       animations: {
            toViewController.view.transform = CGAffineTransform.identity
        },
                       completion: { finished in
            transitionContext.completeTransition(finished)
        })
    }
}

