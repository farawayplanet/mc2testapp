//
//  SlideMenuDissmissAnimation.swift
//  mc2testapp
//
//  Created by Никита Попов on 19/02/2019.
//  Copyright © 2019 Nikita. All rights reserved.
//

import UIKit

class SlideMenuDismissAnimation: NSObject, UIViewControllerAnimatedTransitioning {
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.33
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        guard let fromViewController = transitionContext.viewController(forKey: .from) else {
            assert(false, "controller cannot be nil")
        }
        let containerView = transitionContext.containerView
        let duration = transitionDuration(using: transitionContext)
        
        UIView.animate(withDuration: duration,
                       animations: {
            fromViewController.view.transform = CGAffineTransform(translationX: -containerView.bounds.width, y: 0)
        },
                       completion: { finished in
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
        })
    }
}
