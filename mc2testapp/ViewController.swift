//
//  ViewController.swift
//  mc2testapp
//
//  Created by Никита Попов on 18/02/2019.
//  Copyright © 2019 Nikita. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet private weak var itemNumberLabel: UILabel!
    
    private let transDelegate = SlideMenuTransitioningDelegate()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configSwipe()
    }
    
    private func configSwipe() {
        let swipe = UISwipeGestureRecognizer(target: self,
                                             action: #selector(swipeAction(_:)))
        view.addGestureRecognizer(swipe)
    }

    @IBAction func slideMenuButtonTap(_ sender: Any) {
        showSlideMenu()
    }
    
    @objc private func swipeAction(_ sender: UISwipeGestureRecognizer) {
        showSlideMenu()
    }
    
    private func showSlideMenu() {
        let slideMenu = SlideMenuViewController()
        slideMenu.transitioningDelegate = transDelegate
        slideMenu.modalPresentationStyle = .custom
        slideMenu.delegate = self
        present(slideMenu, animated: true, completion: nil)
    }
}

extension ViewController: SlideMenuDelegate {
    func didTapItem(index: Int) {
        itemNumberLabel.text = "\(index)"
    }
}
