//
//  SlideMenuViewController.swift
//  mc2testapp
//
//  Created by Никита Попов on 18/02/2019.
//  Copyright © 2019 Nikita. All rights reserved.
//

import UIKit

protocol SlideMenuDelegate: class {
    func didTapItem(index: Int)
}

class SlideMenuViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    private let menuItems = SlideMenuDatasource().slideMenuItems()
    
    weak var delegate: SlideMenuDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configSwipe()
        configTable()
    }
    
    private func configSwipe() {
        let swipe = UISwipeGestureRecognizer(target: self,
                                             action: #selector(swipeAction(_:)))
        swipe.direction = .left
        view.addGestureRecognizer(swipe)
    }
    
    private func configTable() {
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.register(UINib.init(nibName: String(describing: SlideMenuTableViewCell.self),
                                      bundle: nil),
                           forCellReuseIdentifier: String(describing: SlideMenuTableViewCell.self))
    }
    
    @objc private func swipeAction(_ sender: UISwipeGestureRecognizer) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func misstapTap(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}

extension SlideMenuViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: SlideMenuTableViewCell = tableView.dequeueCell()

        let viewModel = menuItems[indexPath.row]

        cell.configure(viewModel: viewModel)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        dismiss(animated: true, completion: nil)
        delegate?.didTapItem(index: indexPath.row)
    }
}
