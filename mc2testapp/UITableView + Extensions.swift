//
//  UITableView + Extensions.swift
//  mc2testapp
//
//  Created by Никита Попов on 18/02/2019.
//  Copyright © 2019 Nikita. All rights reserved.
//

import UIKit

extension UITableView {
    func dequeueCell<T: UITableViewCell>() -> T {
        if let cell = self.dequeueReusableCell(withIdentifier: String(describing: T.self)) as? T {
            return cell
        }
        assert(false, "Cell not dequeue")
        return T(style: .default, reuseIdentifier: String(describing: T.self))
    }
}
